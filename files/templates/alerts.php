
    <?php 
        if(isset($_GET['success']) && $_GET['success'] == 'successfulregister') {  
            echo "<p class='alert alert-success' id='alert'>Successfully registered</p>";

         } else if(isset($_GET['error'])) {
            if($_GET['error'] == 'errorlogin') {
                $msg2 = "Wrong credentials";
            } else if($_GET['error'] == 'errorregister') {
                $msg2 = "Name or email has been taken";
            }else if($_GET['error'] == 'noname'){
                $msg2 = "Name is required";
            }else if($_GET['error'] == 'noemail'){
                $msg2 = "Email is required";
            }else if($_GET['error'] == 'nopassword'){
                $msg2 = "Password is required";
            }else if($_GET['error'] == 'invalidpassword'){
                $msg2 = "Password must have lowercase, uppercase, number and special char";
            }else if($_GET['error'] == 'noconfirmpassword'){
                $msg2 = "Password confirmation must be inserted";
            }else if($_GET['error'] == 'nomachpassword'){
                $msg2 = "Passwords don't match";
            } else if($_GET['error'] == 'notype_id'){
                $msg2 = "Type is required";
            }else if($_GET['error'] == 'notunique'){
                $msg2 = "Name or Email not unique";
            }else if($_GET['error'] == 'mincar'){
                $msg2 = "Name must have minimum 3 letters";
            }else if($_GET['error'] == 'invalidusername'){
                $msg2 = "Invalid username";
            }else if($_GET['error'] == 'pleaselogin'){
                $msg2 = "Please log in";
            } else {
                $msg2 = "An error occured";
            }
            echo "<p class='alert alert-error' id='alert'>$msg2</p>";
        }
    ?>

