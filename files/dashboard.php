<?php require "templates/header.php";
      require "../classes/dashboarduser.php";
//Start annd check if sesion is active      
session_start();
if(!isset($_SESSION['id'])) {
    header("Location: home.php");
    die();
}
//creating user object--------
    $task=new \newTask\Showuser();
    $resoult=$task->getobject('* FROM users');
    $tf=$tb=$angular=$react=$php=$node=$vue=$angularjs=$angular2=$react=$symfony=$laravel=$express=$nestjs=$silex=$lumen=0;
    foreach($resoult as $user){
        if($user['id'] == $_SESSION['id']){
            $name=$user['name'];
        }if($user['type_id'] == 1){
            $tf++;
        }if($user['type_id'] == 2){
            $tb++;
        }if($user['subtype_id'] == 1){
            $angular++;
        }if($user['subtype_id'] == 2){
            $react++;
        }if($user['subtype_id'] == 3){
            $php++;
        }if($user['subtype_id'] == 4){
            $node++;
        }if($user['subtype_id'] == 5){
            $vue++;
        }if($user['subsubtype_id'] == 1){
            $angularjs++;
        }if($user['subsubtype_id'] == 2){
            $angular2++;
        }if($user['subsubtype_id'] == 3){
            $react++;
        }if($user['subsubtype_id'] == 4){
            $symfony++;
        }if($user['subsubtype_id'] == 5){
            $laravel++;
        }if($user['subsubtype_id'] == 6){
            $express++;
        }if($user['subsubtype_id'] == 7){
            $nestjs++;
        }if($user['ssubsubtype_id'] == 1){
            $silex++;
        }if($user['ssubsubtype_id'] == 2){
            $lumen++;
        }
    }
    
//creating type opjecs--------
    $t=$task->getobject('* FROM types');
    $subt=$task->getobject('* FROM subtypes');
    $subsubt=$task->getobject('* FROM subsubtypes');
    $ssubsubt=$task->getobject('* FROM ssubsubtypes');    
//Succes masage when user loges in-----    
    if(isset($_GET['success'])) {
        if($_GET['success'] == 'successfullogin'){
            $msg1 = "Hi ".$name.",you have successfully loged in";
        }
        echo "<p class='alert alert-success' id='alert'>$msg1</p>";
    }
//Activating type object---------
$new = file_get_contents('tab.txt');
file_put_contents('tab.txt', "");
$us = json_decode($new,true);

?>
<div class="namesDiv">
    
<?php
//Check if session active -----------
foreach($resoult as $user){
    if($user['id'] == $_SESSION['id']){
        echo "Hi ".$user['name'];
    }
}
//Page content--------------------
?>
</div>
<div class="logsDiv">
<div class="logout"><a href="logout.php" class="out">Logout</a></div>
</div>
<div class="mainhome">
<div class="mleft">
<div class="mld">
    <div class="rtypeDiv">
    <h1>Types of developers</h1>
<?php 
    foreach($t as $type){ ?> 
    <div value="<?php echo  $type['id'] ?>" class="opt typec" style="font-weight:950;font-size:17px" width="100%"><?php echo $type['name'] ?>(<?php if($type['id']==1){ echo $tf; }else if ($type['id']==2){echo $tb;} else{ echo 0 ;}?>)</div>
        <?php 
        foreach($subt as $subtype){ 
            if($subtype['type_id']== $type['id']){ ?>
        <div   value="<?php echo $subtype['id']  ?> " class="opt subtypec" style="font-weight:700;font-size:17px;padding-left:15px;"><?php  echo $subtype['name'] ?>(<?php if($subtype['id']==1){ echo $angular; }else if($subtype['id']==2){echo $react;}else if($subtype['id']==3){echo $php;}else if($subtype['id']==4){echo $node;}else if($subtype['id']==$vue){echo $vue;}else{ echo 0 ;} ?>) </div>
        
        <?php 
            foreach($subsubt as $subsubtype){ 
                if($subsubtype['subtype_id'] == $subtype['id']){ 
                    ?>
            <div  value="<?php  echo $subsubtype['id']; ?>" class="opt subsubtypec" style="font-weight:700;font-size:17px;padding-left:25px;"><?php echo "  ";  echo $subsubtype['name'] ?>(<?php if($subsubtype['id']==1){ echo $angularjs; }else if($subsubtype['id']==2){echo $angular2;}else if($subsubtype['id']==3){echo $react;}else if($subsubtype['id']==4){echo $symfony;}else if($subsubtype['id']== 5){echo $laravel;}else if($subsubtype['id']== 6){echo $express;}else if($subsubtype['id']== 7){echo $nestjs;}else{ echo 0 ;} ?>)</div>
            <?php 
                foreach($ssubsubt as $ssubsubtype){ 
                    if($ssubsubtype['subsubtype_id'] == $subsubtype['id']){ 
                        ?>
                <div  value="<?php  echo  $ssubsubtype['id'] ?>" class="opt ssubsubtypec" style="font-weight:700;font-size:17px; padding-left:50px;"><?php echo "  ";  echo $ssubsubtype['name'] ?>(<?php if($ssubsubtype['id']==1){ echo $silex; }else if($ssubsubtype['id']==2){echo $lumen;}else{ echo 0 ;} ?>)</div>
                <?php
               }
            }
          }
        }
      }
    }
  }
?>
</div>
</div>
</div>
<div class="mright">
<div class="mrd">
<?php if ($us) {  ?> 
 <table class="table">
  <thead>
      <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Type</th>
      </tr>
  </thead>
  <tbody>
     <?php 
     
     foreach($us as $user){?>
     <tr>
         <td><?php echo $user['name'] ?></td>
         <td><?php echo $user['email'] ?></td>
         <?php if( $user['type_id'] == 1){?> <td>Front End Developer</td> <?php } else { ?> <td>Back End Developer</td> <?php }  ?>
     </tr>
     <?php } ?>
  </tbody>

 </table>
<?php } else {?>
<h3 style="color:red">No users under selectet criteria!</h3>
<?php
}
?>
</div>
</div>
</div>
<?php require "templates/footer.php"?>