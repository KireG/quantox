<?php 

namespace newTask;

use PDOException;

require "dataconnection.php";
class Showuser extends Dataconnection {
    
    public function searchFilds($select, $text){

        $text = "%$text%";
        $sql = "SELECT *  FROM users WHERE name or email LIKE :text and type_id LIKE :select;";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(['text'=> $text,'select'=>$select]);
        $stmt->fetchAll(\PDO::FETCH_ASSOC);
       
        try {
            $stmt->execute();
            
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
            
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }  
    }
}