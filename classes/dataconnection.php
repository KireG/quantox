<?php

namespace newTask;

 class Dataconnection {

    private $host = "localhost";
    private $user = "root";
    private $pwd = "";
    private $dbName = "quantox";

    private $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION, 
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, 
        \PDO::ATTR_EMULATE_PREPARES   => false
    ];

    // Connection to database
    protected function connect(){
        try {
            $dsn = "mysql:host=" . $this->host . ";dbname=" . $this->dbName;
            $pdo = new \PDO($dsn, $this->user, $this->pwd, $this->options);
            return $pdo;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }
   //Get method-------------
    public  function getobject($name){
        $sql= "SELECT  $name";
        $statement=$this->connect()->prepare($sql);
        $statement->execute();
        $resoult=$statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resoult;
    
      }

}
?>