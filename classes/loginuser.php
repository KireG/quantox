<?php
 
 namespace newTask;

 use PDOException;

 require "dataconnection.php";

 class Loginuser extends Dataconnection {
     
    public function getuser(){
     //Required fields---------   
        if($_SERVER["REQUEST_METHOD"]== "POST"){
            if(empty($_POST["email"])){
              header("Location:login.php?error=noemail");
            }else if(empty($_POST["password"])){
              header("Location:login.php?error=nopassword");
            }else {
    //Loging in----------------            
            $email = $_POST['email'];
            //$password = password_hash($_POST['password'],PASSWORD_DEFAULT);
            $password=md5($_POST['password']);
            $sql = "SELECT * FROM  users WHERE email= :email and password= :password ";
            $statement= $this->connect()->prepare($sql);
            try{
                $statement->execute(['email'=> $email , 'password'=>$password]);
                $userCount = $statement->rowCount();
                if($userCount > 0){
                    $user=$statement->fetch(\PDO::FETCH_ASSOC);
                    $_SESSION['id'] = $user['id'];
                    header('Location: dashboard.php?success=successfullogin');
                    die();
                }else{
                    header('Location: login.php?error=errorlogin');
                }
            }catch(PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
          }
         }
      }
    }      
 }
