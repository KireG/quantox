<?php

namespace newTask;

use PDOException;

require "dataconnection.php";

class RegisterUser extends Dataconnection {
  
    public function createuser($data){
     //Required fields--------- 
      if($_SERVER["REQUEST_METHOD"]== "POST"){
        if(empty($_POST["name"])){
          header("Location:register.php?error=noname");
        }else if(strlen($_POST["name"])<3){
          header("Location:register.php?error=mincar");
        }else if(preg_match("/[!@#\$%\^&\*]+/", $_POST["name"])
        || preg_match("/ /", $_POST["name"])){
          header("Location:register.php?error=invalidusername");
        }else if(empty($_POST["email"])){
          header("Location:register.php?error=noemail");
        }else if(empty($_POST["password"])){
          header("Location:register.php?error=nopassword");
        }else if(!preg_match("/[a-z]+/", $_POST["password"])
        || !preg_match("/[A-Z]+/", $_POST["password"])
        || !preg_match("/[0-9]+/", $_POST["password"])
        || !preg_match("/[!@#\$%\^&\*]+/", $_POST["password"])){
          header("Location:register.php?error=invalidpassword");
        }else if(empty($_POST["confirmpassword"])){
          header("Location:register.php?error=noconfirmpassword");
        }else if($_POST["password"] !== $_POST["confirmpassword"]){
          header("Location:register.php?error=nomachpassword");
        } else if(empty($_POST["type_id"])){
          header("Location:register.php?error=notype_id");
        }else {
    //Validation of users-------
          $name = $_POST['name'];
          $email = $_POST['email'];
          $sql = "SELECT * FROM  users WHERE name= :name or email= :email ";
          $statement= $this->connect()->prepare($sql);
          $statement->execute(['name'=> $name , 'email'=>$email]);
          $userCount = $statement->rowCount();   
        
          if($userCount > 0 ){
          header("Location:register.php?error=notunique");
          }else{
    //Creating user------------        
        $new_user=[
            "name" => $data["name"],
            "email" =>$data["email"],
            // "password"=>password_hash($_POST['password'],PASSWORD_DEFAULT),
            "password"=>md5($_POST['password']),
            "type_id"=>$data["type_id"],
            "subtype_id"=>$data["subtype_id"],
            "subsubtype_id"=>$data["subsubtype_id"],
            "ssubsubtype_id"=>$data["ssubsubtype_id"],
        ];

          $sql = sprintf(
            "INSERT INTO %s (%s) values (%s)",
            "users",
            implode(", ", array_keys($new_user)),
            ":" . implode(", :", array_keys($new_user))
        );
      
            $statement = $this->connect()->prepare($sql);
            try{
              $statement->execute($new_user);
            }catch (PDOException $e){
              echo $sql . "<br>" . $e->getMessage();
            }
            header('location:login.php?success=successfulregister');
          }
        }
      }
  }
    
}